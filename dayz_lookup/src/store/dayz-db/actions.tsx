import {createAction} from 'redux-api-middleware';
import {
  GET_DAYZ_DATA_REQUEST,
  GET_DAYZ_DATA_SUCCESS,
  GET_DAYZ_DATA_ERROR
} from './types';

export const getData = () => createAction({
  endpoint: `/items.json`,
  method: 'GET',
  types: [
    GET_DAYZ_DATA_REQUEST,
    GET_DAYZ_DATA_SUCCESS,
    GET_DAYZ_DATA_ERROR
  ]
});
