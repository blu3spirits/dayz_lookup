import React from 'react';
import type { MenuProps } from 'antd';
import { Layout, Menu} from 'antd';

import DayZApp from '../../App';

const { Header, Content } = Layout;


const MenuItems: MenuProps['items'] = ['1'].map((key) => ({
  key,
  label: `Desecrated Lands Item Lookup`,
}));

const App: React.FC = () => {
  return (
    <Layout>
      <Header style={{ alignItems: 'left' }}>
        <div className="demo-logo" />
        <Menu theme="dark" items={MenuItems} defaultSelectedKeys={['1']}/>
      </Header>
      <Content style={{ padding: '0 24px', minHeight: 510 }}>
        <DayZApp/>
      </Content>
    </Layout>
  );
};

export default App;

