#!/usr/bin/env python
import pandas as pd
import json
from pandas import testing as tm
KNOWN_DIFFERENT_ITEMS = [
    "brass_knuckles",
    "meat_tenderizer",
    "combat_knife",
    "vsd",
    "boxed_5.56x45mm_rounds",
    "svt_40",
    "water_pistol",
    "boxed_5.45x39mm_tracer_rounds",
    "9rd_deagle_mag",
    "25rd_le_mas_mag",
    "20rd_lar_mag",
    "olga_24_wheel",
    "m1025_front_left_door",
    "m1025_rear_left_door",
    "m1025_front_right_door",
    "m1025_rear_right_door",
    "m1025_hood",
    "m1025_trunk_door",
    "spark_plug",
    "water_gun_magazine",
    "steak_knife",
    "fishing_rods_rack_kit",
    "ski_mask_abnormal",
    "hockey_helmet",
    "skate_helmet_abnormal",
    "fingerless_gloves_abnormal",
    "invisible_bag-400",
    "tactical_vest"
]


def validate_no_dupes(df):
    if len(df.loc[df.duplicated()]):
        raise Exception(f"\n{df.loc[df.duplicated()].item_name.to_string(index=False)}")


def validate(df):
    defined_prices = df.loc[
        df["buy_price"].notnull()
        & df["sell_price"].notnull()
        & ~df["item_name"].isin(KNOWN_DIFFERENT_ITEMS)
    ]
    buy_prices = defined_prices["buy_price"].astype("float")
    sell_prices = defined_prices["sell_price"].astype("float").apply(lambda x: x * 5)
    buy_prices.rename("price", inplace=True)
    sell_prices.rename("price", inplace=True)
    try:
        tm.assert_series_equal(buy_prices, sell_prices)
    except Exception:
        raise Exception(
            df.loc[
                (df["buy_price"].notnull())
                & df["sell_price"].notnull()
                & ~df["item_name"].isin(KNOWN_DIFFERENT_ITEMS)
                & (df["buy_price"] != 5 * df["sell_price"])
            ]
        )


def convert_to_json(df):
    df.index.rename('key', inplace=True)
    new_json = {'results': json.loads(df.reset_index().to_json(orient='records'))}
    with open('../../items.json', 'w') as jf:
        jf.write(json.dumps(new_json))

def main():
    df = pd.read_csv("../../items.csv")
    print("Starting validation")
    validate(df)
    validate_no_dupes(df)
    print("Validation complete")
    convert_to_json(df)


if __name__ == "__main__":
    main()
