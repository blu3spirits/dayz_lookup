# DayZ DB

This project is something my friend and I started to keep track of what we can and cannot
sell on a modded dayz server we play on.

At its core this is really just something that reads from a localized data source
and is index-able quickly in some fashion.

### How are you gathering data
Sadly, screenshots. We have around 250+ screenshots of in-game traders that give us a source of truth on
item name, buy_price, sell_price

### How are you putting that into your index-able data source?
Haha. Manually! OCR didn't want to pick up all the info and we wanted this to be done before the next ice-age
