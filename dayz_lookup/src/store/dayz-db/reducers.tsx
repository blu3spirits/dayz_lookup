import {
  GET_DAYZ_DATA_REQUEST,
  GET_DAYZ_DATA_SUCCESS,
  GET_DAYZ_DATA_ERROR,
  DayzDataActionTypes,
  DayzDataState
} from './types';

const initialState: DayzDataState = {
  loading: true,
  results: [],
  //count: 0,
  error: null
};

const reducer = (state = initialState, action: DayzDataActionTypes): DayzDataState => {
  switch (action.type) {
    case GET_DAYZ_DATA_REQUEST:
      return {...state, loading: true};
    case GET_DAYZ_DATA_SUCCESS: {
      return {
        ...state,
        results: action.payload.results,
        //count: action.payload.results.length,
        loading: false,
        error: null
        }
    }
    case GET_DAYZ_DATA_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };
    default:
      return {
        ...state
      };
  }
};

export default reducer;
