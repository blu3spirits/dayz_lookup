import {createSelector} from 'reselect';
import {RootState} from './types';

export const userSettings = (state: RootState) => state.userSettings;
export const dayzSellables = (state: RootState) => state.dayzDb;

export const hasLightTheme = createSelector(userSettings, settings => settings.lightTheme);
