import {combineReducers} from 'redux';
import userSettings from './user-settings/reducers';
import dayzDb from './dayz-db/reducers'

export default combineReducers({
  userSettings,
  dayzDb
});
