import { useEffect} from 'react';
import DayZTable from './common/components/table';
import {useDispatch, useSelector} from 'react-redux';
import {getData} from './store/dayz-db/actions';
import {dayzSellables} from './store/selectors';
import Loading from './common/components/loading';
import './App.css';

const App = () => {
  const dispatch = useDispatch();

  //Update data on load
  useEffect(() => {
    dispatch(getData());
  }, [dispatch]);

  const sellables = useSelector(dayzSellables);

  return (
    <div>
      { sellables.loading ? (
        <Loading />
      ) : (
        <DayZTable data={sellables.results}/>
      )}
    </div>
  );
};
export default App;
