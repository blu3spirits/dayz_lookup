import {RequestError, ApiError} from 'redux-api-middleware';
import {DayzData} from '../../common/models/data-types'

export const GET_DAYZ_DATA_REQUEST = 'GET_DAYZ_DATA_REQUEST';
export const GET_DAYZ_DATA_SUCCESS = 'GET_DAYZ_DATA_SUCCESS';
export const GET_DAYZ_DATA_ERROR = 'GET_DAYZ_DATA_ERROR';

export interface DayzDataState {
  loading: boolean;
  results: DayzData[];
  //count: number;
  error: Error | null;
}

interface RequestAction {
  type: typeof GET_DAYZ_DATA_REQUEST;
}

interface SuccessAction {
  type: typeof GET_DAYZ_DATA_SUCCESS;
  payload: DayzDataState;
}

interface ErrorAction {
  type: typeof GET_DAYZ_DATA_ERROR;
  payload: RequestError | ApiError;
}

export type DayzDataActionTypes = RequestAction | SuccessAction | ErrorAction;
