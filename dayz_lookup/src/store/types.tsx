import {UserSettingsState} from './user-settings/types';
import {DayzDataState} from './dayz-db/types';

export interface RootState {
  userSettings: UserSettingsState;
  dayzDb: DayzDataState;
}
