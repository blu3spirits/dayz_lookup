export interface DayzData {
  index: number;
  location: string;
  item_name: string;
  buy_price: number | null;
  sell_price: number | null;
}
