import React from 'react';
import { Alert, Space, Spin } from 'antd';

const App: React.FC = () => (
  <Space direction="vertical" style={{ width: '100%' }}>
    <Spin tip="Loading...">
      <Alert
        message="Loading data from remote disk"
        description="Bother Blu to eventually make this run with a real back-end!"
        type="info"
      />
    </Spin>
  </Space>
);

export default App;

