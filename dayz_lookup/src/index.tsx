import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import configureStore from './store/configure-store';
import {Provider} from 'react-redux';
//import App from './App';
import DayZHeader from './common/components/dayzdbheader';
import reportWebVitals from './reportWebVitals';

const store = configureStore();

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

const AppWithRedux = () => {
  return (
    <React.StrictMode>
      <Provider store={store}>
        <DayZHeader />
      </Provider>
    </React.StrictMode>
  );
};
root.render(
  <AppWithRedux />
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
